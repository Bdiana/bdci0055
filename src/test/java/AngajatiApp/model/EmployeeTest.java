package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EmployeeTest {

    private Employee emp1;
    private Employee emp2;
    private Employee emp3;
    private Employee emp4;

    @Before
    public void setUp() throws Exception {

        emp1 = new Employee("Dana", "Baciu", "2453675121212", DidacticFunction.TEACHER, 4000.0);
        emp2 = new Employee("Alina", "Demian", "2049294202312", DidacticFunction.CONFERENTIAR, 3500.9);
        emp3 = new Employee("Cristian", "Grigore", "394592302405", DidacticFunction.ASISTENT, 2000.0);
        emp4 = new Employee("Alin", "Mera" ,"847239202104", DidacticFunction.ASISTENT, 3729.0);
    }

    @Test
    public void getLastName() {
        assertEquals("Baciu", emp1.getLastName());
        assertNotEquals("Demian", emp3.getLastName());
    }

    @Test
    public void setFirstName() {
        emp4.setFirstName("Maria");
        assertEquals("Maria", emp4.getFirstName());
    }

    @Test
    public void constructor() {

        Employee emp5 = new Employee();
        assertEquals("", emp5.getFirstName());
        assertEquals("", emp5.getLastName());
        assertEquals("", emp5.getCnp());
        assertEquals(DidacticFunction.ASISTENT, emp5.getFunction());
        assertEquals(Double.valueOf(0), emp5.getSalary());
    }
    @Test(timeout = 150)
    public void setCnp(){
        try{
            Thread.sleep(100);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        emp1.setCnp("201212112121");
        assertEquals("201212112121", emp1.getCnp());

    }

    @After
    public void tearDown() throws Exception {
    }
}